package com.example.sunead;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuneadApplication {

    public static void main(String[] args) {
        SpringApplication.run(SuneadApplication.class, args);
    }

}
